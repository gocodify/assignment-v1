package main

import (
	"encoding/json"
	"log"
	"net/http"
	"net/url"

	"github.com/gorilla/mux"
)

func getContactsHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	contacts := make([]Contact, 0)
	gormDatabase.Model(&Contact{}).Where(&Contact{
		UserID: session.Values["userID"].(uint),
	}).Find(&contacts)

	sendResponse(w, true, contacts)
}

func getContactHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	contactID := mux.Vars(r)["contactID"]
	if contactID == "" {
		sendResponse(w, false, errServer)
		return
	}

	cID, err := getUintIDFromString(contactID)
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	var contact Contact
	gormDatabase.Model(&Contact{}).Where(&Contact{
		ID:     cID,
		UserID: session.Values["userID"].(uint),
	}).Find(&contact)

	sendResponse(w, true, contact)
}

func deleteContactHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	contactID := mux.Vars(r)["contactID"]
	if contactID == "" {
		sendResponse(w, false, errServer)
		return
	}

	cID, err := getUintIDFromString(contactID)
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	var contact Contact
	gormDatabase.Model(&Contact{}).Where(&Contact{
		ID:     cID,
		UserID: session.Values["userID"].(uint),
	}).Find(&contact)
	if contact.ID == 0 {
		sendResponse(w, false, errServer)
		return
	}

	gormDatabase.Model(&Contact{}).Delete(&contact)

	sendResponse(w, true, "Contact Deleted Successfully.")
}

func postContactHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	var contact Contact
	if err := json.NewDecoder(r.Body).Decode(&contact); err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	if contact.FirstName == "" ||
		contact.LastName == "" ||
		contact.Organization == "" ||
		contact.Website == "" ||
		contact.Email == "" ||
		contact.Phone == "" {

		sendResponse(w, false, "All fields marked(*) are required.")
		return
	}

	if _, err := url.ParseRequestURI(contact.Website); err != nil {
		log.Println(err)
		sendResponse(w, false, "Website URL is not valid.")
		return
	}

	contact.UserID = session.Values["userID"].(uint)

	gormDatabase.Model(&Contact{}).Create(&contact)

	sendResponse(w, true, "Contact created successfully.")
}

func putContactHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	contactID := mux.Vars(r)["contactID"]
	if contactID == "" {
		sendResponse(w, false, errServer)
		return
	}

	cID, err := getUintIDFromString(contactID)
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	var existingContact Contact
	gormDatabase.Model(&Contact{}).Where(&Contact{
		ID:     cID,
		UserID: session.Values["userID"].(uint),
	}).Find(&existingContact)
	if existingContact.ID == 0 {
		sendResponse(w, false, errServer)
		return
	}

	var contact Contact
	if err := json.NewDecoder(r.Body).Decode(&contact); err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	if contact.FirstName == "" ||
		contact.LastName == "" ||
		contact.Organization == "" ||
		contact.Website == "" ||
		contact.Email == "" ||
		contact.Phone == "" {

		sendResponse(w, false, "All fields marked(*) are required.")
		return
	}

	if _, err := url.ParseRequestURI(contact.Website); err != nil {
		log.Println(err)
		sendResponse(w, false, "Website URL is not valid.")
		return
	}

	gormDatabase.Model(&Contact{}).Save(&contact)

	sendResponse(w, true, "Contact updated successfully.")
}
