angular.module('app').config(['$locationProvider', '$stateProvider', '$urlRouterProvider', 'NotificationProvider', function($locationProvider, $stateProvider, $urlRouterProvider, NotificationProvider) {
    $locationProvider.html5Mode(true)
    
    $urlRouterProvider.otherwise('/')

    NotificationProvider.setOptions({
        replaceMessage: true
    })

    $stateProvider
    .state('signOut', {
        onEnter: function() {
            window.location = window.location.origin + '/api/v1/sign-out'
        }
    })

    .state('home', {
        url: '/',
        redirectTo: 'contacts'
    })

    .state('contacts', {
        url: '/contacts',
        redirectTo: 'contacts.view'
    })

    .state('contacts.view', {
        url: '',
        templateUrl: 'public/partials/contacts-view.html',
        controller: 'ContactViewController as ctrl'
    })

    .state('contacts.create', {
        url: '/create',
        templateUrl: 'public/partials/contact-create.html',
        controller: 'ContactCreateController as ctrl'
    })

    .state('contacts.edit', {
        url: '/edit/:contactID',
        templateUrl: 'public/partials/contact-edit.html',
        controller: 'ContactEditController as ctrl'
    })
}])
