angular.module('app').controller('ContactEditController', ['MainService', 'Notification', '$stateParams', '$state', function(MainService, Notification, $stateParams, $state) {
    var _self = this

    _self.formData = {}

    _self.contactID = $stateParams.contactID
    
    MainService.getContact(_self.contactID).then(function(result) {
        if (result.Ok) {
            _self.formData = result.Data
        } else {
            Notification.error(result.Data)
        }
    })

    _self.requestInProgress = false
    _self.editContact = function() {
        _self.requestInProgress = true

        MainService.editContact(_self.formData, _self.contactID).then(function(result) {
            if (result.Ok) {
                Notification(result.Data)
                $state.go('contacts')
            } else {
                Notification.error(result.Data)
            }

            _self.requestInProgress = false
        })
    }
}])
