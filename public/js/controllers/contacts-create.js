angular.module('app').controller('ContactCreateController', ['MainService', 'Notification', '$state', function(MainService, Notification, $state) {
    var _self = this

    _self.formData = {
        'FirstName': '',
        'LastName': '',
        'Phone': '',
        'Email': '',
        'Organization': '',
        'Website': '',
    }

    _self.requestInProgress = false
    _self.createContact = function() {
        _self.requestInProgress = true

        MainService.createContact(_self.formData).then(function(result) {
            if (result.Ok) {
                Notification(result.Data)
                $state.go('contacts')
            } else {
                Notification.error(result.Data)
            }

            _self.requestInProgress = false
        })
    }
}])
