angular.module('app').controller('ContactViewController', ['MainService', 'Notification', '$state', function(MainService, Notification, $state) {
    var _self = this

    _self.contacts = []

    _self.requestInProgress = true

    MainService.getContacts().then(function(result) {
        if (result.Ok) {
            _self.contacts = result.Data

            for (i = 0; i < _self.contacts.length; i ++) {
                _self.contacts[i]['DeletingInProgress'] = false
            }
        } else {
            Notification.error(result.Data)
        }

        _self.requestInProgress = false
    })

    _self.deleteContact = function(contact, index) {
        contact.DeletingInProgress = true

        MainService.deleteContact(contact.ID).then(function(result) {
            contact.DeletingInProgress = false
            if (result.Ok) {
                _self.contacts.splice(index, 1)

                Notification(result.Data)
            } else {
                Notification.error(result.Data)
            }
        })
    }
}])
