angular.module('app').controller('MainXController', ['MainService', '$scope', '$transitions', '$state', '$timeout', function(MainService, $scope, $transitions, $state, $timeout) {
	var _self = this

	_self.me = {}

	MainService.getMe().then(function(result) {
		if (result.Ok) {
			_self.me = result.Data
		}
	})
}])
