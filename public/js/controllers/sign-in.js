angular.module('app').controller('SignInController', ['MainService', 'Notification', '$state', function(MainService, Notification, $state) {
    var _self = this
    
    _self.requestInProgress = false
    
    _self.formData = {
        'Email': '',
        'Password': ''
    }

    _self.submit = function() {
        _self.requestInProgress = true

        MainService.signIn(_self.formData).then(function(result) {
            if (result.Ok) {
                window.location.reload()
            } else {
                Notification.error(result.Data)

                _self.requestInProgress = false
            }
        })
    }
}])
