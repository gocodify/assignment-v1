angular.module('app').service('MainService', ['$http', function($http) {
	var _self = this

	_self.signIn = function(formData) {
		return $http.post('/api/v1/sign-in', formData).then(function(response) {
			return response.data
		})
	}
	
	_self.signUp = function(formData) {
		return $http.post('/api/v1/sign-up', formData).then(function(response) {
			return response.data
		})
	}

	_self.getMe = function() {
		return $http.get('/api/v1/get-me',).then(function(response) {
			return response.data
		})
	}

	_self.getContacts = function() {
		return $http.get('/api/v1/contacts',).then(function(response) {
			return response.data
		})
	}

	_self.getContact = function(contactID) {
		return $http.get('/api/v1/contacts/' + contactID,).then(function(response) {
			return response.data
		})
	}

	_self.createContact = function(formData) {
		return $http.post('/api/v1/contacts', formData).then(function(response) {
			return response.data
		})
	}

	_self.editContact = function(formData, contactID) {
		return $http.put('/api/v1/contacts/' + contactID, formData).then(function(response) {
			return response.data
		})
	}

	_self.deleteContact = function(contactID) {
		return $http.delete('/api/v1/contacts/' + contactID).then(function(response) {
			return response.data
		})
	}
}])
