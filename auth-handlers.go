package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"

	"github.com/badoux/checkmail"
	"github.com/gorilla/sessions"
	"golang.org/x/crypto/bcrypt"
)

func signUpHandler(w http.ResponseWriter, r *http.Request) {
	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}
	r.Body.Close()

	if user.Email == "" || user.Password == "" || user.FirstName == "" || user.LastName == "" {
		sendResponse(w, false, "All fields marked (*) are required")
		return
	}

	user.Email = strings.ToLower(user.Email)
	if err := checkmail.ValidateFormat(user.Email); err != nil {
		log.Println(err)
		sendResponse(w, false, "Email address is not valid.")
		return
	}

	var existingUser User
	gormDatabase.Model(&User{}).Where(&User{
		Email: user.Email,
	}).Find(&existingUser)
	if existingUser.ID != 0 {
		sendResponse(w, false, "This email address is already registered with us.")
		return
	}

	b, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Fatal(err)
	}
	user.Password = string(b)

	gormDatabase.Model(&User{}).Create(&user)

	sendResponse(w, true, "Sign up successful.")
}

func signInHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	var user User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}
	r.Body.Close()

	log.Println(user)

	if user.Email == "" || user.Password == "" {
		sendResponse(w, false, "All fields are required.")
		return
	}

	var userX User
	gormDatabase.Where(&User{
		Email: user.Email,
	}).First(&userX)
	if userX.ID == 0 {
		sendResponse(w, false, "This email address is not registered with us.")
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(userX.Password), []byte(user.Password)); err != nil {
		if err != bcrypt.ErrMismatchedHashAndPassword {
			log.Println(err)
		}
		sendResponse(w, false, "Password is not valid")
		return
	}

	session.Values["userID"] = userX.ID

	session.Options.HttpOnly = true

	session.Save(r, w)

	sendResponse(w, true, "Sign in successful.")
}

func signOutHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
	}

	session.Options = &sessions.Options{
		MaxAge:   -1,
		Path:     "/",
		HttpOnly: true,
	}

	session.Save(r, w)

	http.Redirect(w, r, "/", http.StatusSeeOther)
}
