package main

import "time"

// Response ...
type Response struct {
	Ok   bool
	Data interface{}
}

// User ...
type User struct {
	CreatedAt time.Time  `gorm:"not null"`
	DeletedAt *time.Time `gorm:"index"`
	ID        uint       `gorm:"primary_key;auto_increment;not null"`
	UpdatedAt time.Time  `gorm:"not null"`
	FirstName string
	LastName  string
	Password  string
	Email     string
}

// Contact ...
type Contact struct {
	CreatedAt    time.Time  `gorm:"not null"`
	DeletedAt    *time.Time `gorm:"index"`
	ID           uint       `gorm:"primary_key;auto_increment;not null"`
	UpdatedAt    time.Time  `gorm:"not null"`
	FirstName    string
	LastName     string
	Email        string
	Phone        string
	Organization string
	Website      string
	UserID       uint
}
