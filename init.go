package main

import (
	"flag"
	"log"

	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var (
	gormDatabase *gorm.DB
	sessionStore = sessions.NewCookieStore([]byte("the-road-ahead-engage"))
)

const (
	errServer = "Something went wrong. Please contact your system administrator."
)

func init() {
	log.SetFlags(log.Ldate | log.Lmicroseconds | log.Lshortfile)

	flag.String("port", "", "port to listen on")
	flag.String("env", "", "environment (can be either dev or prod)")

	flag.Parse()

	if flag.Lookup("port").Value.String() == "" {
		log.Fatal("-port is required")
	}

	if flag.Lookup("env").Value.String() == "" || (flag.Lookup("env").Value.String() != "dev" && flag.Lookup("env").Value.String() != "prod") {
		log.Fatal("-env is required. It can be either dev or prod.")
	}

	var err error
	gormDatabase, err = gorm.Open("mysql", "assignment_user:mypassword@tcp(localhost:3306)/assignment?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		log.Fatal(err)
	}

	gormDatabase.AutoMigrate(&User{})
	gormDatabase.AutoMigrate(&Contact{})
}
