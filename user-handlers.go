package main

import (
	"log"
	"net/http"
)

func getMeHandler(w http.ResponseWriter, r *http.Request) {
	session, err := sessionStore.Get(r, "user-session")
	if err != nil {
		log.Println(err)
		sendResponse(w, false, errServer)
		return
	}

	var user User
	gormDatabase.Model(&User{}).Where(&User{
		ID: session.Values["userID"].(uint),
	}).Find(&user)

	sendResponse(w, true, user)
}
