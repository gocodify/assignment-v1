package main

import (
	"flag"
	"log"
	"net/http"

	"github.com/NYTimes/gziphandler"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
)

func main() {
	router := mux.NewRouter()

	router.HandleFunc("/", indexHandler).Methods("GET")

	router.HandleFunc("/api/v1/sign-up", signUpHandler).Methods("POST")
	router.HandleFunc("/api/v1/sign-in", signInHandler).Methods("POST")
	router.HandleFunc("/api/v1/sign-out", signOutHandler).Methods("GET")

	router.HandleFunc("/api/v1/get-me", getMeHandler).Methods("GET")

	router.HandleFunc("/api/v1/contacts", getContactsHandler).Methods("GET")
	router.HandleFunc("/api/v1/contacts/{contactID}", getContactHandler).Methods("GET")
	router.HandleFunc("/api/v1/contacts", postContactHandler).Methods("POST")
	router.HandleFunc("/api/v1/contacts/{contactID}", putContactHandler).Methods("PUT")
	router.HandleFunc("/api/v1/contacts/{contactID}", deleteContactHandler).Methods("DELETE")

	router.NotFoundHandler = http.HandlerFunc(indexHandler)

	router.PathPrefix("/public/").Handler(http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))))

	flag.VisitAll(func(flag *flag.Flag) {
		log.Println(flag.Name, "->", flag.Value)
	})

	if flag.Lookup("env").Value.String() == "dev" {
		server := http.Server{
			Addr:    ":" + flag.Lookup("port").Value.String(),
			Handler: cors.Default().Handler(gziphandler.GzipHandler(noCacheMW(router))),
		}

		if err := server.ListenAndServe(); err != nil {
			log.Fatal(err)
		}

	} else {
		server := http.Server{
			Addr:    ":" + flag.Lookup("port").Value.String(),
			Handler: cors.Default().Handler(gziphandler.GzipHandler(noCacheMW(router))),
		}

		go func() {
			log.Fatal(http.ListenAndServe(":80", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				target := "https://" + r.Host
				http.Redirect(w, r, target, http.StatusMovedPermanently)
			})))
		}()

		if err := server.ListenAndServeTLS("/etc/letsencrypt/live/dev.gocodify.com/fullchain.pem", "/etc/letsencrypt/live/dev.gocodify.com/privkey.pem"); err != nil {
			log.Fatal(err)
		}
	}
}

func noCacheMW(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Cache-Control", "no-store")

		h.ServeHTTP(w, r)
	})
}
