package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func sendResponse(w http.ResponseWriter, ok bool, data interface{}) {
	response := Response{
		Ok:   ok,
		Data: data,
	}

	if err := json.NewEncoder(w).Encode(response); err != nil {
		log.Println(err)
	}
}

func getUintIDFromString(id string) (uint, error) {
	u64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		return 0, err
	}

	return uint(u64), nil
}
